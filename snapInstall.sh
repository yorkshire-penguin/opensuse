#!/bin/bash

# This is to get snap on a fresh install

sudo zypper addrepo --refresh https://download.opensuse.org/repositories/system:/snappy/openSUSE_Tumbleweed snaps

sudo zypper --gpg-auto-import-keys refresh

sudo zypper dup --from snaps

sudo zypper install snapd

sudo systemctl enable snapd
sudo systemctl start snapd

sudo systemctl enable snapd.apparmor
sudo systemctl start snapd.apparmor


