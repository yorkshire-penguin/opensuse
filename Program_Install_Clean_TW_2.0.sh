#!/bin/bash

# Add gpg
 
echo "ADDING GPG KEY"
sleep 5s
 
gpg --full-gen-key

echo "GPG PROCESS COMPLETE"
sleep 5s

#Change system name

echo "CHANGING SYSTEM HOSTNAME"
sleep 5s

sudo systemctl hostname set-hostname TW-desktop

echo "HOSTNAME CHANGED"
sleep 5s

# Add packman repo

echo "ADDING PACKMAN REPO"
sleep 5s

sudo zypper ar -cfp 90 --allow-vendor-change http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman

echo "PACKMAN REPO ADDED"
sleep 5s

#Add needed programs after clean install

echo "INSTALLING REQUIRED PROGRAMMES FROM REPOS"
sleep 5s

sudo zypper install -y latte-dock git smplayer htop kdeconnect-kde terminator exfat-utils fuse-exfat 

echo "INSTALL COMPLETE"
sleep 5s

# Add calibre from source

echo "INSTALLING CALIBRE FROM SOURCE"
sleep 5s

sudo zypper -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

echo "CALIBRE INSTALL COMPLETE"
sleep 5s

#Add docker package

echo "INSTALLING DOCKER"

sudo zypper install -y docker

echo "INSTALL OF DOCKER COMPLETE"
sleep 5s

#Sublime text3 Install

echo "INSTALLING SUBLIME TEXT 3"
sleep 5s

sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg

sudo zypper addrepo -g -f https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

sudo zypper install -y sublime-text

echo "SUBLIME TEXT 3 INSTALLED"
sleep 5s

#Gaming Packages

echo "INSTALLING REQUIRED PROGRAMMES FOR GAMES"
sleep 5s

sudo zypper install -y wine wine-32bit wine-gecko wine-mono winetricks 
sudo zypper install -y steam lutris 
 
echo "REQUIRED PROGRAMMES FOR GAMES INSTALLED"
sleep 5s

#open ports for kde connect

echo "OPEN FIREWALL PORTS FOR KDE CONNECT"
sleep 5s

sudo firewall-cmd --zone=public --permanent --add-port=1714-1764/tcp
sudo firewall-cmd --zone=public --permanent --add-port=1714-1764/udp

echo "PORTS OPEN"
sleep 5s

#Amend bahrc file

echo "AMEDING BASHRC FILE"
sleep 5s

echo "Additional aliases" >> ~/.bashrc
echo alias upsys='sudo zypper ref && sudo zypper up' >> ~/.bashrc
echo alias upflat='flatpak update' >> ~/.bashrc
echo alias upall='sudo zypper ref && sudo zypper dup && flatpak update' >> ~/.bashrc

echo "BASHRC AMENDED"
sleep 5s

#Install flatpak

echo "INSTALLING FLATPAK" 
sleep 5s

sudo zypper install -y flatpak

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak update

echo "FLATPAK INSTALLED"
sleep 5s

#Install flatpak packages

echo "INSTALLING FLATPAK PACKAGES"
sleep 5s


echo "FLATPAKS PACKAGES INSTALLED"
sleep 5s


# Install games

echo "INSTALLING GAMES"
sleep 5s

sudo zypper install -y extreme-tuxracer 
sudo zypper install -y supertuxkart
sudo zypper install -y supertux2
sudo zypper install -y xonotic

echo "GAMES ARE INSTALLED"
sleep 5s

#Update system

echo "UPDATING SYSTEM"
sleep 5s

sudo zypper ref && sudo zypper dup

echo "UPDATE COMPLETE"
sleep 5s


#Pull down folders saved to cloud on to local system

#Pull down configs for programs


echo "##########################################################"
echo "#################### TASKS COMPLETE ######################"
echo "##########################################################"
