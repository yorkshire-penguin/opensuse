#!/bin/bash

# Add gpg
 
echo "ADDING GPG KEY"
sleep 5s
 
gpg --full-gen-key

echo "GPG PROCESS COMPLETE"
sleep 5s

#Change system name

echo "CHANGING SYSTEM HOSTNAME"
sleep 5s

sudo systemctl hostname set-hostname leap-desktop

echo "HOSTNAME CHANGED"
sleep 5s

# Add packman repo

echo "ADDING PACKMAN REPO"
sleep 5s

sudo zypper ar -cfp 90 --allow-vendor-change http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_15.2/ packman

echo "PACKMAN REPO ADDED"
sleep 5s

#Add needed programs after clean install

echo "INSTALLING REQUIRED PROGRAMMES FROM REPOS"
sleep 5s

sudo zypper install -y latte-dock git smplayer htop kdeconnect-kde terminator exfat-utils fuse-exfat 

echo "INSTALL COMPLETE"
sleep 5s

#Gaming Packages

echo "INSTALLING REQUIRED PROGRAMMES FOR GAMES"
sleep 5s

sudo zypper install -y wine wine-32bit wine-gecko wine-mono winetricks 
sudo zypper install -y steam lutris 
 
echo "REQUIRED PROGRAMMES FOR GAMES INSTALLED"
sleep 5s

#open ports for kde connect

echo "OPEN FIREWALL PORTS"
sleep 5s

sudo firewall-cmd --zone=public --permanent --add-port=1714-1764/tcp
sudo firewall-cmd --zone=public --permanent --add-port=1714-1764/udp

echo "PORTS OPEN"
sleep 5s

#Amend bahrc file

echo "AMEDING BASHRC FILE"
sleep 5s

echo "Additional aliases" >> ~/.bashrc
echo alias upsys='sudo zypper ref && sudo zypper up' >> ~/.bashrc
echo alias upflat='flatpak update' >> ~/.bashrc

echo "BASHRC AMENDED"
sleep 5s

#Install flatpak

echo "INSTALLING FALTPAK"
sleep 5s

sudo zypper install -y flatpak

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak update

echo "FALTPAK INSTALLED"
sleep 5s

#Install flatpak packages

echo "INSTALLING FALTPAK PACKAGES"
sleep 5s

flatpak install -y telegram 
flatpak install -y calibre

echo "FLATPAKS PACKAGES INSTALLED"
sleep 5s

#Update system

echo "UPDATING SYSTEM"
sleep 5s

sudo zypper ref && sudo zypper up

echo "UPDATE COMPLETE"
sleep 5s

# Install games

echo "INSTALLING GAMES"
sleep 5s

sudo zypper install -y extreme-tuxracer 
sudo zypper install -y supertuxkart
sudo zypper install -y supertux2

echo "GAMES ARE INSTALLED"
sleep 5s

#Pull down folders saved to cloud on to local system

#Pull down configs for programs

#Pull down Atom from website and configure, from flatpak??



 
echo "##########################################################"
echo "#################### TASKS COMPLETE ######################"
echo "##########################################################"
